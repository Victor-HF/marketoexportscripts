// Imports
const fetch = require("node-fetch");
const config = require('./config');

// Authenticate
const getAccessToken = async () => {

    let accessToken;

    try {

        accessToken = await fetch(config.authenticationEndpoint)

            .then(response => response.json())

            .then(jsonResponse => {

                if (jsonResponse.error) {
                    throw new Error('There was a problem getting the access token \n');
                }

                let jsonResponseObject = {
                    access_token: jsonResponse.access_token,
                    token_type: jsonResponse.token_type,
                    expires_in: jsonResponse.expires_in,
                };

                //console.log('Reponse: \n', JSON.stringify(jsonResponse, stringifyReplacer, stringifySpace), '\n');
                return jsonResponseObject.access_token;

            })

    } catch (error) {
        //console.error(error);
        throw error;
    };

    return accessToken;
};

module.exports = { getAccessToken }