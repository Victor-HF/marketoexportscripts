require("dotenv").config();

const instanceID = process.env.instanceID;
const clientID = process.env.clientID;
const clientSecret = process.env.clientSecret;
// const folderName = 'Lead Database';

module.exports = { 
    authenticationEndpoint: `${instanceID}/identity/oauth/token?grant_type=client_credentials&client_id=${clientID}&client_secret=${clientSecret}`,
    // folderIdByName: `${instanceID}/rest/asset/v1/folder/byName.json?client_id=${clientID}&client_secret=${clientSecret}&name=${folderName}&workSpace=Default`,
    browseFoldersById: `${instanceID}/rest/asset/v1/folders.json?client_id=${clientID}&client_secret=${clientSecret}&maxDepth=20&maxReturn=200`,
    browseSmartListsByFolderId: `${instanceID}/rest/asset/v1/smartLists.json?client_id=${clientID}&client_secret=${clientSecret}&maxReturn=200`,
    getListInfoById: `${instanceID}/rest/asset/v1/smartList/{id}.json?client_id=${clientID}&client_secret=${clientSecret}&includeRules=true`
 }