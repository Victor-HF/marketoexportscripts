function myErrorLogger(reason) {
    console.log(reason);
    throw new Error(`${reason}`);
}

function resultLogger(result) {
    console.log(JSON.stringify(result, null, 1))
}

module.exports = { myErrorLogger, resultLogger }