// Imports
/// npm
const fetch = require("node-fetch");

/// files
const helper = require('./helperFunctions');
const auth = require('./authentication');
const config = require('./config');

// get folders to browse in
const getFolderIds = async (rootFolderId) => {

    // console.log('Requesting access...');

    let access_token;

    try {
        access_token = await auth.getAccessToken();
    } catch (error) {
        console.log(error?.message);
        throw error;
    };

    //console.log(`Confirming access token: ${access_token}`)

    console.log('Getting Folder Ids for search...');

    let folderIds = await fetch(config.browseFoldersById + `&access_token=${access_token}` + `&root=${rootFolderId}`)
        .then(response => response.json())
        .then(jsonResponse => {
            //console.log('API usage: \n', JSON.stringify(jsonResponse, stringifyReplacer, stringifySpace), '\n');
            if (jsonResponse?.success === false) {
                throw jsonResponse?.errors[0]?.message;
            }
            return jsonResponse.result
                .map(element => {
                    return ({
                        id: element.folderId.id,
                        path: element.path,
                        type: element.folderId.type
                    }) 
                });
        })
        .catch(Error);

    console.log('Folder IDs stored...');
    //helper.resultLogger(folderIds);

    //return [JSON.parse(rootFolderId), ...folderIds];
    return folderIds;

    //return [rootFolderId, ...folderIds];
}


// get smart list ids
const getSmartListIds = async (folderId) => {

    // console.log('Requesting access...');

    let access_token;

    try {
        access_token = await auth.getAccessToken();
    } catch (error) {
        console.log(error?.message);
        throw error;
    };

    //console.log(`Confirming access token: ${access_token}`)

    console.log('Getting Smart Lists from folder...');

    let smartListIds = await fetch(config.browseSmartListsByFolderId + `&access_token=${access_token}` + `&folder={"id":${folderId.id},"type":${folderId.type}}`)
        .then(response => response.json())
        .then(jsonResponse => {
            //console.log('API usage: \n', JSON.stringify(jsonResponse, stringifyReplacer, stringifySpace), '\n');
            if (jsonResponse?.success === false) {
                throw jsonResponse?.errors[0]?.message;
            }
            return jsonResponse.result
                .map(element => {
                    return ({
                        id: element.id,
                        name: element.name
                    });
                });
        })
        .catch(Error);

    // console.log('Smart List Ids requested...');
    // helper.resultLogger(smartListIds);

    return smartListIds;
}


// get smart list info
const getSmartListInfo = async (smartListId) => {

    // console.log('Requesting access...');

    let access_token;

    try {
        access_token = await auth.getAccessToken();
    } catch (error) {
        console.log(error?.message);
        throw error;
    };

    //console.log(`Confirming access token: ${access_token}`)

    let smartListInfo = await fetch(config.getListInfoById.replace('{id}',smartListId) + `&access_token=${access_token}`)
        .then(response => response.json())
        .then(jsonResponse => {
            //console.log('API usage: \n', JSON.stringify(jsonResponse, stringifyReplacer, stringifySpace), '\n');
            if (jsonResponse?.success === false) {
                throw jsonResponse?.errors[0]?.message;
            }
            return jsonResponse.result
                .map(element => ({
                        id: element.id,
                        name: element.name,
                        createdAt: element.createdAt,
                        url: element.url,
                        rules: element.rules
                }));
        })
        .catch(Error);

    // console.log('Smart List Ids requested...');
    helper.resultLogger(smartListInfo);

    return smartListInfo;
}


const exportSmartListInfo = async (rootFolderId) => {

    let folderIds = await getFolderIds(rootFolderId);
    console.log(folderIds)

    let smartListIds = [];

    for (folderId of folderIds) {

        let progressDenom = folderIds.length;
        let progressNomin = folderIds.indexOf(folderId) + 1;
        let progress = Math.floor((progressNomin / progressDenom) * 100);

        console.log(`[Folder ID: ${folderId.id}]`);
        console.log(`Starting search at path: \n${folderId.path} \n`);

        let result = await getSmartListIds(folderId);


        if (!result) {

            console.log(`No lists found in folder`); // check with James, never land here, typeof element won't even display either... 

            return

        };

        Array.from(result).forEach(element => {

            // console.log(element)

            console.log(`Adding new-found list ID: ${element.id}`);

            smartListIds.push(element);

        });

        console.log(`Progress: ${progress} % - [${progressNomin}/${progressDenom}]\n`)

    };

    let smartListInfo = [];

    for (smartListId of smartListIds) {

        let progressDenom = smartListIds.length;
        let progressNomin = smartListIds.indexOf(smartListId) + 1;
        let progress = Math.floor((progressNomin / progressDenom) * 100);

        console.log(`Getting info for: ${smartListId.name} - [Smart List ID: ${smartListId.id}]\n`);


        let result = await getSmartListInfo(smartListId.id);

        Array.from(result).forEach(element => {

            // console.log(typeof element);

            if (!element) {

                // console.log(`No lists found in folder`);

            } else {

                smartListInfo.push(element);

            };

        });

        console.log(`Progress: ${progress} % - [${progressNomin}/${progressDenom}]\n`);

    };

    // console.log('Smart List info \n' + JSON.stringify(smartListInfo, null, 1));
    // console.log('Smart List info \n' + smartListInfo);

    return smartListInfo;

};

const rootFolderId = '{"id":3425, "type":"Program"}'; 
// folder = 12156 for tests in UAT 
// last folder in test structure = 12165
// test folder for programs 3425

// UAT
// Database - 1 --> default 5
// Design studio - 2
// Marketing activities - 3 --> default: 19

exportSmartListInfo(rootFolderId);

// NEED TO: getfolder ids needs to get also type, in export smart lists, don't reduce and pass the whole object, then in getsmartlist ids, use both id and type (make tipe dynamic)

